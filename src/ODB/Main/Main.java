package Main;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import javax.swing.text.DateFormatter;

import org.neodatis.odb.ODB;
import org.neodatis.odb.ODBFactory;
import org.neodatis.odb.Objects;
import org.neodatis.odb.core.query.IQuery;
import org.neodatis.odb.core.query.criteria.Where;
import org.neodatis.odb.impl.core.query.criteria.CriteriaQuery;

import Neodatis_Models.Competiciones;
import Neodatis_Models.Participantes;

public class Main {
	
	
	
	public static void connectarBD() throws IOException {

		    String sharedFolder="NeodatisServer";
		    String path="smb://13.93.26.136/"+sharedFolder+"myBD.odb";
	}
	private static final String sharedFolder="NeodatisServer";
	private static final String path="smb://13.93.26.136/"+sharedFolder+"myBD.odb";
		private static final Scanner sc = new Scanner(System.in);
		private static final String ODB_NAME = path;
		public static void main(String[] args) throws Exception {
			int i=0;
			Scanner sc=new Scanner(System.in);
			do{
				System.out.println("1. Almacenar objetos");
				System.out.println("2. Listar objectos");
				System.out.println("3. Actualizar objetos");
				System.out.println("4. Eliminar objetos");
				System.out.println("5. Sortir");
				i=sc.nextInt();
				switch(i){
					case 1: saveData();
							break;
					case 2: listJugadorss();
							break;
					case 3: updateJugadors();
							break;
					case 4: deleteJugadors();
							break;
					case 5: break;
					default: System.out.println("Opci�n desconocida");
				}
				if (i==5){
					System.out.println("Bye bye");
					break;
				}
			}while (true);
			sc.close();
		}
			
		public static void saveData() throws Exception {
	        // Create 4 Jugadors
	        Participantes Jugadors1 = new Participantes("olivier", "atom","titus");
	        Participantes Jugadors2 = new Participantes("pierre", "larson","lars");
	        Participantes Jugadors3 = new Participantes("elohim", "bye","elo");
	        Participantes Jugadors4 = new Participantes("minh", "manas","mina");
	        List<Participantes> participantes = new ArrayList();
	        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	    	Calendar cal = Calendar.getInstance();
	    	participantes.add(Jugadors1);
	    	participantes.add(Jugadors2);
	    	participantes.add(Jugadors3);
	    	participantes.add(Jugadors4);
	    	 //current time in milliseconds
	        long currentDateTime = System.currentTimeMillis();
	       
	        //creating Date from millisecond
	        Date currentDate = new Date(currentDateTime);


	        Competiciones competi = new Competiciones(1,"Red bull",dateFormat.format(cal),participantes);
	        ODB odb = null;
	        
	        try {
	            // Open the database
	            odb = ODBFactory.open(ODB_NAME);
	 
	            // Store the object
	            odb.store(Jugadors1);
	            odb.store(Jugadors2);
	            odb.store(Jugadors3);
	            odb.store(Jugadors4);
	            odb.store(competi);
	        } finally {
	            if (odb != null) {
	                // Close the database
	                odb.close();
	            }
	        }
		}
		public static void listJugadorss() throws Exception {
			 
	        ODB odb = null;
	 
	        try {
	            // Open the database
	            odb = ODBFactory.open(ODB_NAME);
	            Iterator<Object> jugadors= odb.getObjects(Participantes.class).iterator();
	 
	            int i = 1;
	            // display each object
	            while (jugadors.hasNext()) {
	            	Participantes jugador = (Participantes)jugadors.next();
	                System.out.println((i++) + "\t: " + jugador.getNombre()+" "+jugador.getApodo());
	            }
	            Iterator<Object> competiciones= odb.getObjects(Participantes.class).iterator();
	       	 
	            int y = 1;
	            // display each object
	            while (competiciones.hasNext()) {
	            	Competiciones competi = (Competiciones)competiciones.next();
	                System.out.println((y++) + "\t: " + competi.getNombre()+" "+competi.getFecha());
	            }
	        } finally {
	            if (odb != null) {
	                // Close the database
	                odb.close();
	            }
	        }
	    }
		public static void updateJugadors() throws Exception {
			ODB odb = null;

			try {
				// Open the database
				odb = ODBFactory.open(ODB_NAME);
				IQuery query = new CriteriaQuery(Participantes.class, Where.equal("nom", "olivier"));

				Objects<Participantes> Jugadorss = odb.getObjects(query);

				// Gets the first Jugadors (there is only one!)
				Participantes Jugadorstochange = Jugadorss.getFirst();

				// Changes the name
				if (Jugadorstochange!=null){
					System.out.println("Introdueix el jugador a actualitzas");
					Jugadorstochange.setNombre(sc.nextLine());
					// Actually updates the object
					odb.store(Jugadorstochange);
				}
				

			} finally {
				if (odb != null) {
					// Close the database
					odb.close();
				}
			}
		}
		public static void deleteJugadors() throws Exception {

			ODB odb = null;

			try {
				// Open the database
				odb = ODBFactory.open(ODB_NAME);
				System.out.println("Introdueix el jugador a eliminar");
				IQuery query = new CriteriaQuery(Participantes.class, Where.like("nom", sc.nextLine()));
				Participantes deleteJugadors = (Participantes)odb.getObjects(query).getFirst();
				// Gets the first Jugadors (there is only one!)
				odb.delete(deleteJugadors);
			}
			catch(IndexOutOfBoundsException e){
				System.out.println("No hay datos de este usuario");

			} finally {
				if (odb != null) {
					// Close the database
					odb.close();
				}
			}
		}
	}
