package Neodatis_Models;

 
public class Participantes implements java.io.Serializable {

	private Integer id;
	private String nombre;
	private String apellidos;
	private String apodo;
	private String comentario;

	public Participantes() {
	}

	public Participantes(String nombre, String apellidos, String apodo) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.apodo = apodo;
	}

	public Participantes(String nombre, String apellidos, String apodo, String comentario) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.apodo = apodo;
		this.comentario = comentario;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getApodo() {
		return this.apodo;
	}

	public void setApodo(String apodo) {
		this.apodo = apodo;
	}

	public String getComentario() {
		return this.comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

}
