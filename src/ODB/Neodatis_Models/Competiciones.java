package Neodatis_Models;

import java.util.Date;
import java.util.List;

public class Competiciones{
	private int ID;
	private String Nombre;
	private String fecha;
	private List<Participantes> participantes;
	public int getID() {
		return ID;
	}
	
	public Competiciones(int iD, String nombre, String string, List<Participantes> participantes) {
		ID = iD;
		Nombre = nombre;
		this.fecha = string;
		this.participantes = participantes;
	}

	public void setID(int iD) {
		ID = iD;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public List<Participantes> getParticipantes() {
		return participantes;
	}
	public void setParticipantes(List<Participantes> participantes) {
		this.participantes = participantes;
	}
	@Override
	public String toString() {
		return "Competiciones [ID=" + ID + ", Nombre=" + Nombre + ", fecha=" + fecha + ", participantes="
				+ participantes + "]";
	}
	
}