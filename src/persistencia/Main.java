package persistencia;

import java.util.Scanner;

import JDBC.jdbcManager;
import hibernateConMapeo.persistencia.hbmManager2;
import hibernateSinMapeo.persistencia.hbmManager;

public class Main {
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		menu();

	}
	
public static void menu() {
		
		
		System.out.println("1: JDBC");
		System.out.println("2: Hibernate Sin Mapeo");
		System.out.println("3: Hibernate Con Mapeo");
		System.out.println("4: Neodatis");
		System.out.println("5: Salir");
		String respuesta = sc.nextLine();
		
		switch(respuesta) {
		case "1":{
			jdbcManager.main(null);
			break;
		}
		case "2":{
			hbmManager.main(null);
			break;
		}
		
		case "3":{
			hbmManager2.main(null);
			break;
		}
		
		case "4":{
			ODB.Main.Main.main(null);
			break;
		}
		
		case "5":{
			System.out.println("Hasta la vista!");
			break;
		}
		
		default:{
			System.out.println("Eso no es ni 1 ni 2 ni 3 ni 4 ni 5...");
			menu();
			break;
		}
		
		}
		
	}
}
