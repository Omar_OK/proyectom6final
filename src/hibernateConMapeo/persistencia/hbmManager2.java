package hibernateConMapeo.persistencia;

import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import hibernateConMapeo.beans.Participantes;
import hibernateConMapeo.persistencia.hibernateutil;
import persistencia.Main;

public class hbmManager2 {
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		menu();

	}
	
public static void menu() {
		
		
		System.out.println("1: Crear Participantes");
		System.out.println("2: Modificar Participantes");
		System.out.println("3: Eliminar Participantes");
		System.out.println("4: Listar Participantess");
		System.out.println("5: Volver");
		String respuesta = sc.nextLine();
		
		switch(respuesta) {
		case "1":{
			createParticipantes();
			break;
		}
		case "2":{
			modifyParticipantes();
			break;
		}
		
		case "3":{
			deleteParticipantes();
			break;
		}
		
		case "4":{
			listParticipantes();
			break;
		}
		
		case "5":{
			Main.menu();
			break;			
		}
		
		default:{
			System.out.println("Eso no es ni 1 ni 2 ni 3 ni 4 ni 5...");
			menu();
			break;
		}
		
		}
		
	}

public static void createParticipantes() {
	SessionFactory sesion = hibernateutil.getSessionFactory();
	
	Session session = sesion.openSession();
	Transaction tx = session.beginTransaction();
	System.out.println("Insereixo un participant nou.");
	Participantes parto = new Participantes("Carlos", "Carloso Carlosez", "ElCarlo", "Hippiti Hoppiti representando al barrio");
	
	session.save(parto);
	tx.commit();
	session.close();
	
	menu();
}

public static void modifyParticipantes() {
	SessionFactory sesion = hibernateutil.getSessionFactory();
	
	Session session = sesion.openSession();
	Transaction tx = session.beginTransaction();
	System.out.println("Introduce el id del Participante a ser modificado:");
	int codSoc = Integer.parseInt(sc.nextLine());
	Participantes Participantes = (Participantes)session.get(Participantes.class, codSoc);
	
	System.out.println("Nombre: ");
	Participantes.setNombre(sc.nextLine());
	
	
	System.out.println("Apellidos: ");
	Participantes.setApellidos(sc.nextLine());
	
	System.out.println("Apodo: ");
	Participantes.setApodo(sc.nextLine());
	
	System.out.println("Comentario");
	Participantes.setComentario(sc.nextLine());
	

	
	session.update(Participantes);
	tx.commit();
	session.close();
	
	menu();
	

}

public static void deleteParticipantes() {
	SessionFactory sesion = hibernateutil.getSessionFactory();
	
	Session session = sesion.openSession();
	Transaction tx = session.beginTransaction();
	System.out.println("Introduce el id del Participante a ser eliminado:");
	int codSoc = Integer.parseInt(sc.next());
	Participantes parto = (Participantes)session.get(Participantes.class, codSoc);
	session.delete(parto);
	tx.commit();
	session.close();
	String respuesta = sc.nextLine();
	System.out.print(respuesta);
	
	menu();
	
}

public static void listParticipantes() {
	SessionFactory sesion = hibernateutil.getSessionFactory();
	Session session = sesion.openSession();
	List<Participantes> Participantess = session.createCriteria(Participantes.class).list();
	for(Participantes Participantes: Participantess) {
	System.out.println(Participantes.getId() +" || "+ Participantes.getNombre() +" || "+ Participantes.getApellidos() +" || "+ Participantes.getApodo() +" || "+ Participantes.getComentario() +"\n");
	}
	menu();
}

}
