package hibernateConMapeo.persistencia;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class hibernateutil {

	private static final SessionFactory sessionFactory;
		
		static {
		
			try {
				java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.OFF);
	            sessionFactory = new Configuration().configure("/hibernateConMapeo/hibernate.cfg.xml").buildSessionFactory();
			}
			catch(Throwable ex){
				System.err.println("Initial SessionFactory creation failed" + ex);
				throw new ExceptionInInitializerError(ex);
				
			}
	}
		
		public static SessionFactory getSessionFactory() {
			return sessionFactory;
			
		}		
}
