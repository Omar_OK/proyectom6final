package JDBC;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.mysql.jdbc.Connection;

public class jdbcManager {

	public static Connection conn;
	public static Scanner sc;

	public static void main(String[] args) {
		menu();
	}

	public static void menu() {
		try {

			// Comprobamos si el driver de MySQL esta
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException ex) {
				System.out.println("Error al registrar el driver de MySQL: " + ex);
			}

			// Si el Driver esta disponible creamos la conexion
			conn = (Connection) DriverManager.getConnection("jdbc:mysql://168.63.14.96:41063/RedSocial", "root", ""); // Ejecutar el servidor myXampp3!!

			System.out.println("Conexió a base de datos correcta. Engegant el programa...");
			System.out.println("-----------------------------------------------------------");

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}

		sc = new Scanner(System.in);
		int opcion = -1;

		while (opcion != 6) {
			System.out.println("(1) Registrar usuario.");
			System.out.println("(2) Modificar datos de un usuario existente.");
			System.out.println("(3) Eliminar usuario.");
			System.out.println("(4) Ver todos los usuario.");
			System.out.println("(5) Consultar datos de un usuario.");
			System.out.println("(6) Salir.");
			opcion = sc.nextInt();

			switch (opcion) {
			case 1: {
				newUser();
				break;
			}
			case 2: {
				listAllUsers();
				System.out.println("ID de usuario: ");
				int id = sc.nextInt();
				modifyUser(id);
				break;
			}
			case 3: {
				removeUser();
				break;
			}
			case 4: {
				listAllUsers();
				break;
			}
			case 5: {
				listAllUsers();
				System.out.println("Id del usuario a consultar:");
				checkUser(sc.nextInt());
				break;
			}
			case 6: {
				System.out.println("Sortint..");
				break;
			}
			default: {
				System.out.println("Opció no reconeguda.");
			}
			}
		}
	}

	// Metodo para registrar a usuarios nuevos
	private static void newUser() {
		sc.nextLine();

		System.out.println("Nombre de usuario:");
		String username = sc.nextLine();

		System.out.println("Contraseña:");
		String pw = sc.nextLine();

		System.out.println("Nombre:");
		String nombre = sc.nextLine();

		System.out.println("Apellidos:");
		String apellidos = sc.nextLine();

		System.out.println("Imagen:");
		String img = sc.nextLine();

		// Cridem al metode que ens proporciona el codi que pertoca al nou user
		int nuevoID = obteneridUser();

		// Creem la frase que insertarem amb els valors proporcionats
		String sqlInsert = "insert into Users " + "values (" + nuevoID + ",'" + username + "','" + pw + "','" + nombre
				+ "','" + apellidos + "', NULL)";

		try {
			// Creem i executem l'ordre
			Statement stmt = (Statement) conn.createStatement();
			int count = stmt.executeUpdate(sqlInsert);
			System.out.println("Insertades " + count + " files.");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// Metodo para modificar a usuarios 
	private static void modifyUser(int id) {
		checkUser(id);
		System.out.println("Nuevo nombre: ");
		String nuevoNombre = sc.next();
		System.out.println("Nuevos apellidos: ");
		String nuevoApellidos = sc.next();

		try {
			String query = "update Users set NombreReal = " + "'" + nuevoNombre + "'" + " where ID = " + id;
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			String query = "update Users set Apellidos = " + "'" + nuevoApellidos + "'" + " where ID = " + id;
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Nombre y apellidos actualizados.");

	}
	// Metodo para comprobar a usuarios 
	private static void checkUser(int id) {
		// Consultem unicament el usuari amb el codi facilitat
		String sql = "select * from Users where ID = " + id;

		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// Assignem l'informacio a les variables
			while (rs.next()) {
				int idUser = rs.getInt("ID");
				String nombre = rs.getString("NombreReal");
				String apellidos = rs.getString("Apellidos");
				System.out.println(idUser + "|" + nombre + "|" + apellidos + "|");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// Metodo para borrar a usuarios 
	private static void removeUser() {
		listAllUsers();
		System.out.println("Id del usuario a eliminar:");
		int id = sc.nextInt();

		checkUser(id);
		System.out.println("Estas segur de querer eliminar el usuario " + id + "? 'SI' o 'NO'");
		String eliminarUsuario = sc.next();

		if (eliminarUsuario.equalsIgnoreCase("SI")) {
			try {
				Statement stmt = conn.createStatement();
				String query = "delete from Users where ID = " + id;
				stmt.executeUpdate(query);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("Usuario eliminat.");
		}
	}
	// Metodo para listar a todos usuarios 
	private static void listAllUsers() {
		String sql = "select * from Users";
		try {
			Statement stmt = (Statement) conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int idUser = rs.getInt("ID");
				String nombre = rs.getString("NombreReal");
				String apellidos = rs.getString("Apellidos");
				System.out.println(idUser + "|" + nombre + "|" + apellidos + "\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// Metodo para obtener el id por si no fuera autoincrementable
	private static int obteneridUser() {
		String sql = "select * from Users";

		try {
			Statement stmt = (Statement) conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			int max = Integer.MIN_VALUE;
			while (rs.next()) {
				int idUser = rs.getInt("ID");
				if (idUser > max) {
					max = idUser;
				}
			}
			return max + 1;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

}
